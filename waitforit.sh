#!/usr/bin/env bash

function usage() {
	echo "Usage: $0 -m maxtime -u url" 1>&2
	echo "" 1>&2
	echo "  m: the max time in seconds to wait" 1>&2
	echo "  u: the url to request" 1>&2
	echo "  d: the delay between attempts; default is 3 seconds" 1>&2  
	exit 1
}

while getopts ":m:u:h" opt; do
	case $opt in
		m) maxtime=$OPTARG ;;
		u) url=$OPTARG ;;
		d) delay=$OPTARG ;;
		h) usage ;;
		\?) echo "Invalid option: -$OPTARG" >&2 ;;
		:) echo "Option -$OPTARG requires an argument." >&2; exit 1;;
	esac
done

if [ -e $url ]; then echo "url is required"; exit 1; fi
if [ -e $maxtime ]; then echo "maxtime is required"; exit 1; fi
if [ -e $delay ]; then delay=3; fi

echo "Calling ${url} for a maximum ${maxtime} seconds to test availability"

STARTTIME=$(date +%s)
TDIFF=0
ATTEMPT=1
while [ $TDIFF -lt $maxtime ]; do
	echo "Attempt #$ATTEMPT elapsed time $TDIFF seconds."
	curl -skfm $maxtime $url
	if [ $? -eq 0 ]; then
		echo "Received successful response"
		exit 0;
	fi
	sleep $delay
	(( TDIFF = $(date +%s) - STARTTIME ))
	(( ATTEMPT = ATTEMPT + 1 ))
done
echo "Failed to receive successful response $maxtime seconds"
exit 1
